# ytdl

This script takes a youtube video URL & downloads it's corresponding audio file in selected formats

## Requirements
* Python 3
* [download](https://pypi.org/project/download/)

## Installation

clone this repository
`git clone https://gitlab.com/kskarthik/ytdl`

Download the library using pip
`pip3 install download`


## Usage:

`./ytdl <audio quality> <youtube video url>`

Example: `./ytdl webm-160 https://youtube.com/watch?v=U51MSK6nSQE`

Download resume is supported in case of interruption

### Supported audio formats:
Apple music formats
* m4a-48 ( 48kbps)
* m4a-128 (128 Kbps)
* m4a-256 (256 Kbps)

Open format supported by most web browsers & popular audio players

* webm-50 (50 Kbps)
* webm-70 (70 Kbps)
* webm-160 (160 Kbps)